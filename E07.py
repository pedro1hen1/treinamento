# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@pedro1hen1'


def ex01():
    """Faça um Programa que peça dois números e imprima o maior deles."""
    a = float(input("Insira o primeiro numero "))
    b = float(input("Insira o segundo numero "))

    if a > b:
        print("O maior numero inserido ->", a)

    elif a > b:
        print("O menor numero inserido ->", b)
    else:
        print("Numeros %d e %d são iguais" % (a, b))


def ex02():
    """Faça um Programa que peça um valor e mostre na tela se o valor é
    positivo ou negativo."""
    a = float(input("Insira um valor "))
    if a < 0:
        print("%d -> valor negativo" % a)
    else:
        print("%d -> valor positivo" % a)


def ex03():
    """Faça um Programa que verifique se uma letra digitada é "F" ou "M".
    Conforme a letra escrever: F - Feminino, M - Masculino, Sexo Inválido."""

    sexo = input("Insira o sexo F ou M ")

    if sexo == 'F' or sexo == 'f':
        print("F - Feminino")
    elif sexo == 'M' or sexo == 'm':
        print("M - Masculino")
    else:
        print("Sexo Inválido")


def ex04():
    """Faça um Programa que verifique se uma letra digitada é vogal ou
    consoante."""
    letra = input("Insira uma letra ")
    lista = ['a', 'e', 'i', 'o', 'u']

    if letra in lista:
        print(letra, "é uma vogal")
    else:
        print(letra, "é uma consoante")


def ex05():
    """Faça um programa para a leitura de duas notas parciais de um aluno.
    O programa deve calcular a média alcançada por aluno e apresentar:
    A mensagem "Aprovado", se a média alcançada for maior ou igual a sete;
    A mensagem "Reprovado", se a média for menor do que sete;"""
    n1 = float(input("Insira nota 1 "))
    n2 = float(input("Insira nota 2 "))

    media = (n1 + n1) / 2
    if media >= 7 and media < 10:
        print("Aprovado")
    elif media < 7:
        print("Reprovado")
    else:
        print("Aprovado com Distinção")


def ex06():
    """Faça um Programa que leia três números e mostre o maior deles."""
    n1 = float(input("Digite o primeiro valor: "))
    n2 = float(input("Digite o segundo valor: "))
    n3 = float(input("Digite o terceiro valor: "))
    maior = max([n1, n2, n3])
    print("Maior numero inserido ->", maior)


def ex07():
    """Faça um Programa que leia três números e mostre o maior e o menor deles."""
    n1 = float(input("Digite o primeiro valor: "))
    n2 = float(input("Digite o segundo valor: "))
    n3 = float(input("Digite o terceiro valor: "))
    maior = max([n1, n2, n3])
    menor = min([n1, n2, n3])
    print("_" * 30)
    print("Maior numero inserido ->", maior)
    print("Menor numero inserido ->", menor)


def ex08():
    """Faça um programa que pergunte o preço de três produtos e informe qual
    produto você deve comprar, sabendo que a decisão é sempre pelo mais barato."""

    n1 = float(input("Insira o primeiro valor R$ "))
    n2 = float(input("Insira o primeiro valor R$ "))
    n3 = float(input("Insira o primeiro valor R$ "))
    menor = min([n1, n2, n3])
    print("O produto de menor valor que deve ser comprado custa R$", menor)


def ex09():
    """Faça um Programa que leia três números e mostre-os em ordem decrescente."""
    n1 = float(input("Insira o primeiro número "))
    n2 = float(input("Insira o segundo número  "))
    n3 = float(input("Insira o terceiro número : "))
    ordem = [n1, n2, n3]
    ordem.sort(key=float, reverse=True)
    print("decrescente: ", ordem)


def ex10():
    """Faça um Programa que pergunte em que turno você estuda. Peça para
    digitar M-matutino ou V-Vespertino ou N- Noturno. Imprima a mensagem
    "Bom Dia!", "Boa Tarde!" ou "Boa Noite!" ou "Valor Inválido!",
    conforme o caso."""
    print("Em qual turno você estuda?")
    turno = input("M = Matutino, V = Vespertino, N = Noturno\n")
    if turno == 'M' or turno == 'm':
        print("Bom Dia")
    elif turno == 'V' or turno == 'v':
        print("Boa Tarde")
    elif turno == 'N' or turno == 'n':
        print("Boa Noite")
    else:
        print("Valor Inválido!")


def ex11():
    """As Organizações Tabajara resolveram dar um aumento de salário aos seus
    colaboradores e lhe contrataram para desenvolver o programa que
    calculará os reajustes.
    Faça um programa que recebe o salário de um colaborador e o reajuste
    segundo o seguinte critério, baseado no salário atual:
    salários até R$ 280,00 (incluindo) : aumento de 20%
    salários entre R$ 280,00 e R$ 700,00 : aumento de 15%
    salários entre R$ 700,00 e R$ 1500,00 : aumento de 10%
    salários de R$ 1500,00 em diante : aumento de 5% Após o aumento ser
    realizado, informe na tela:
    o salário antes do reajuste;
    o percentual de aumento aplicado;
    o valor do aumento;
    o novo salário, após o aumento."""

    salario = float(input("Insira seu salario\n"))
    valor20 = salario * 0.20
    final20 = salario * 1.20
    valor15 = salario * 0.15
    final15 = salario * 1.15
    valor10 = salario * 0.10
    final10 = salario * 1.10
    valor5 = salario * 0.05
    final5 = salario * 1.05

    print("Antes Reajuste: ", salario)

    if salario <= 280:
        print("Aumento: 20%")
        print("Valor: ", valor20)
        print("Final: ", final20)
    elif salario > 200 and salario <= 700:
        print("Aumento: 15%")
        print("Valor: ", valor15)
        print("Final: ", final15)
    elif salario > 700 and salario <= 1500:
        print("Aumento: 10%")
        print("Valor: ", valor10)
        print("Final: ", final10)
    else:
        print("Aumento: 5%")
        print("Valor: ", valor5)
        print("Final: ", final5)


def ex12():
    """Faça um programa para o cálculo de uma folha de pagamento, sabendo que
    os descontos são do Imposto de Renda, que depende do salário bruto
    (conforme tabela abaixo) e 3% para o Sindicato e que o FGTS corresponde
    a 11% do Salário Bruto, mas não é descontado (é a empresa que deposita).
    O Salário Líquido corresponde ao Salário Bruto menos os descontos.
    O programa deverá pedir ao usuário o valor da sua hora e a quantidade de
    horas trabalhadas no mês.
    Desconto do IR:
    Salário Bruto até 900 (inclusive) - isento
    Salário Bruto até 1500 (inclusive) - desconto de 5%
    Salário Bruto até 2500 (inclusive) - desconto de 10%
    Salário Bruto acima de 2500 - desconto de 20% Imprima na tela as informações,
    dispostas conforme o exemplo abaixo. No exemplo o valor da hora é
    5 e a quantidade de hora é 220.
            Salário Bruto: (5 * 220)        : R$ 1100,00
            (-) IR (5%)                     : R$   55,00
            (-) INSS ( 10%)                 : R$  110,00
            FGTS (11%)                      : R$  121,00
            Total de descontos              : R$  165,00
            Salário Liquido                 : R$  935,00
    """
    hora = float(input("Insira quanto você ganha por hora "))
    horas_mes = int(input("Insira quantas horas você trabalha no mês "))
    salario = horas_mes * hora
    ir5 = salario * 0.05
    ir10 = salario * 0.10
    ir20 = salario * 0.20
    inss = salario * 0.10
    fgts = salario * 0.11
    print("salario Bruto: ", salario)

    if salario <= 900:
        print("Seu salario não teve alteração")
    else:
        print("INSS: ", inss)
        print("FGTS: ", fgts)

    if salario > 900 and salario <= 1500:
        salario_liquido = float(salario) - float(ir5) - float(inss)
        print("IR 5%: ", ir5)
        print("Salario Liquido: ", salario_liquido)
    elif salario > 1500 and salario <= 2500:
        salario_liquido = float(salario) - float(ir10) - float(inss)
        print("IR 10%: ", ir10)
        print("Salario Liquido: ", salario_liquido)

    else:
        salario_liquido = float(salario) - float(ir20) - float(inss)
        print("IR 20%: ", ir20)
        print("Salario Liquido: ", salario_liquido)


def ex13():
    """Faça um Programa que leia um número e exiba o dia correspondente da
    semana. (1-Domingo, 2- Segunda, etc.), se digitar outro valor deve aparecer
    valor inválido"""
    dia = int(input("Insira o dia da semana de 1 a 7\n "))

    if dia == 1:
        print("Domingo")
    elif dia == 2:
        print("Segunda")
    elif dia == 3:
        print("Terça")
    elif dia == 4:
        print("Quarta")
    elif dia == 5:
        print("Quinta")
    elif dia == 6:
        print("Sexta")
    elif dia == 7:
        print("Sabado")
    else:
        print("Valor Inválido")


def ex14():
    """Faça um programa que lê as duas notas parciais obtidas por um aluno numa
    disciplina ao longo de um semestre, e calcule a sua média. A atribuição de
    conceitos obedece à tabela abaixo:
    Média de Aproveitamento  Conceito
    Entre 9.0 e 10.0        A
    Entre 7.5 e 9.0         B
    Entre 6.0 e 7.5         C
    Entre 4.0 e 6.0         D
    Entre 4.0 e zero        E
    O algoritmo deve mostrar na tela as notas, a média, o conceito
    correspondente e
    a mensagem “APROVADO” se o conceito for A, B ou C ou “REPROVADO”
    se o conceito for D ou E."""
    n1 = float(input("Insira nota 1 "))
    n2 = float(input("Insira nota 2 "))
    media = (n1 + n2) / 2

    if media >= 0 and media <= 4:
        print("Nota 1  |", n1)
        print("Nota 2  |", n2)
        print("Media   |", media)
        print("Conceito|E")
        print("Reprovado")
    elif media > 4 and media <= 6:
        print("Nota 1  |", n1)
        print("Nota 2  |", n2)
        print("Media   |", media)
        print("Conceito|D")
        print("Reprovado")
    elif media > 6 and media <= 7.5:
        print("Nota 1  |", n1)
        print("Nota 2  |", n2)
        print("Media   |", media)
        print("Conceito|C")
        print("Aprovado")
    elif media > 7.5 and media <= 9:
        print("Nota 1  |", n1)
        print("Nota 2  |", n2)
        print("Media   |", media)
        print("Conceito|B")
        print("Aprovado")
    else:
        print("Nota 1  |", n1)
        print("Nota 2  |", n2)
        print("Media   |", media)
        print("Conceito|A")
        print("Aprovado")


def ex15():
    """Faça um Programa que peça os 3 lados de um triângulo. O programa deverá
    informar se os valores podem ser um triângulo. Indique, caso os lados
    formem um triângulo, se o mesmo é: equilátero, isósceles ou escaleno.
    Dicas:
    Três lados formam um triângulo quando a soma de quaisquer dois lados
    for maior que o terceiro;
    Triângulo Equilátero: três lados iguais;
    Triângulo Isósceles: quaisquer dois lados iguais;
    Triângulo Escaleno: três lados diferentes;"""

    print("Insira os lados do Triangulo")
    l1 = float(input("Lado 1: "))
    l2 = float(input("Lado 2: "))
    l3 = float(input("Lado 3: "))
    print("_" * 30)

    if l1 + l2 > l3 or l1 + l3 > l2 or l2 + l3 > l1:
        print("É um Triangulo")
        if l1 == l2 and l1 == l3:
            print("Equilatero")
        elif l1 == l2 or l2 == l3 or l3 == l1:
            print("Isóceles")
        else:
            print("Escaleno")
    else:
        print("Não é um Triangulo")


def ex16():
    """Faça um programa que calcule as raízes de uma equação do segundo grau, na
    forma ax2 + bx + c. O programa deverá pedir os valores de a, b e c e fazer as
    consistências, informando ao usuário nas seguintes situações:
    Se o usuário informar o valor de A igual a zero, a equação não é do segundo
    grau e o programa não deve fazer pedir os demais valores, sendo encerrado;
    Se o delta calculado for negativo, a equação não possui raizes reais. Informe
    ao usuário e encerre o programa;
    Se o delta calculado for igual a zero a equação possui apenas uma raiz real;
    informe-a ao usuário;
    Se o delta for positivo, a equação possui duas raiz reais;
    informe-as ao usuário;"""

    import math

    print("Equação do 2ª grau = ax² + bx + c")

    a = int(input("Insira o valor de A "))

    if (a == 0):
        print("Se A=0,Você informou A=0\n A equação não é do segundo")
    else:
        b = int(input("Insira o valor de B "))
        c = int(input("Insira o valor de C "))
        delta = b * b - (4 * a * c)

        if delta < 0:
            print("Delta menor que 0.\n A equação não possui raizes reais")
        elif delta == 0:
            raiz = -b / (2 * a)
            print("Delta calculado = 0 ,possui apenas uma raiz sendo ela")
            print("Raiz = ", raiz)
        else:
            raiz1 = (-b + math.sqrt(delta)) / (2 * a)
            raiz2 = (-b - math.sqrt(delta)) / (2 * a)
            print("Delta Positivo")
            print("Raizes: ", raiz1, "e", raiz2)


def ex17():
    """Faça um Programa que peça um número correspondente a um determinado ano
    e em seguida informe se este ano é ou não bissexto."""
    ano = int(input("Insira o ano\n"))
    if (ano % 4 == 0 and ano % 100 != 0) or ano % 400 == 0:
        print("O ano de ", ano, "é Bissexto")
    else:
        print("O ano de ", ano, "Não Bissexto")


def ex18():
    """Faça um Programa que peça uma data no formato dd/mm/aaaa e
    determine se a mesma é uma data válida."""

    data = input("Insira a data conforme o formato dd/mm/aaaa ")
    if len(data) != 10:
        print("Inserido de forma incorreta")
        print("Invalido")
    else:
        if data[2] != '/' or data[5] != '/':
            print("Inserido de forma incorreta")
            print("Invalido")
        else:
            numeros_data = data.split('/')
            if int(numeros_data[0]) > 31:
                print("Inserido de forma incorreta")
                print("Invalido")
            elif int(numeros_data[1]) > 12:
                print("Inserido de forma incorreta")
                print("Invalido")
            else:
                print(data, "Formato correto")
                print("Data Valida")


def ex19():
    """Faça um Programa que leia um número inteiro menor que 1000 e
    imprima a quantidade de centenas, dezenas e unidades do mesmo.
    Observando os termos no plural a colocação do "e", da
    vírgula entre outros. Exemplo:
    326 = 3 centenas, 2 dezenas e 6 unidades
    12 = 1 dezena e 2 unidades Testar com: 326, 300, 100, 320, 310,305, 301,
    101, 311, 111, 25, 20, 10, 21, 11, 1, 7 e 16
    """

    import math
    numero = int(input("Insira um numero Menor ou Igual a 1000\n"))
    print("_" * 50)
    if numero <= 1000:
        centena = numero / 100
        centena2 = math.floor(float(centena))
        resto_centena = centena - centena2
        resto_multiplicado = resto_centena * 100

        dezena = resto_multiplicado / 10
        dezena2 = math.floor(float(dezena))
        unidade = dezena - dezena2
        unidade_certo = unidade * 10

        print(numero, "=", centena2, "Centena(s)", dezena2, "dezena(s): ", round(unidade_certo), "Unidade(s): ")
    else:
        print("Você inseriu um numero maior que 1000")


def ex20():
    """--EXERCICIO REPETIDO--Faça um Programa para leitura de três notas
    parciais de um aluno.
    programa deve calcular a média alcançada por aluno e presentar:
    A mensagem "Aprovado", se a média for maior ou igual a 7, com a
    respectiva média alcançada;
    A mensagem "Reprovado", se a média for menor do que 7, com a
    respectiva média alcançada;
    A mensagem "Aprovado com Distinção", se a média for igual a 10.
    """
    n1 = float(input("Insira nota 1 "))
    n2 = float(input("Insira nota 2 "))

    media = (n1 + n1) / 2
    if media >= 7 and media < 10:
        print("Aprovado")
    elif media < 7:
        print("Reprovado")
    else:
        print("Aprovado com Distinção")


def ex21():
    """Faça um Programa para um caixa eletrônico. O programa deverá perguntar
    ao usuário a valor do saque e depois informar quantas notas de cada valor
    serão fornecidas. As notas disponíveis serão as de 1, 5, 10, 50 e 100 reais
     O valor mínimo é de 10 reais e o máximo de 600 reais. O programa não deve
     se preocupar com a quantidade de notas existentes na máquina.
    Exemplo 1: Para sacar a quantia de 256 reais, o programa fornece duas notas
    de 100, uma nota de 50, uma nota de 5 e uma nota de 1;
    Exemplo 2: Para sacar a quantia de 399 reais, o programa fornece três notas
    de 100, uma nota de 50, quatro notas de 10, uma nota de 5 e quatro notas de 1.
    """
    print("Valor mínimo para saque: R$10,00.")
    print("Valor máximo para saque: R$600,00")
    valor = int(input("Qual valor deseja sacar: R$"))

    while (valor < 10) or (valor > 600):
        print("O valor excede o valor de saque")
        valor = int(input("Digite novamente: R$"))

    if (valor >= 10) and (valor <= 600):
        resto1 = valor % 100
        resto2 = resto1 % 50
        resto3 = resto2 % 10
        resto4 = resto3 % 5

    nota100 = valor // 100
    nota50 = resto1 // 50
    nota10 = resto2 // 10
    nota5 = resto3 // 5
    nota1 = resto4 // 1
    print("R$100,00:", nota100)
    print("R50,00:", nota50)
    print("R$10,00:", nota10)
    print("R$5,00:", nota5)
    print("R$1,00:", nota1)


def ex22():
    """Faça um Programa que peça um número inteiro e determine se ele é
    par ou impar. Dica: utilize o operador módulo (resto da divisão)."""

    numero = int(input("Insira um número inteiro "))
    if numero % 2 == 0:
        print("O numero", numero, "é um número par")
    else:
        print("O numero", numero, "é um número Impar")


def ex23():
    """Faça um Programa que peça um número e informe se o número é
    inteiro ou decimal. Dica: utilize uma função de arredondamento."""

    numero = float(input("Insira um número "))

    if (numero // 1 == numero):
        print("O número", int(numero), "é um numero inteiro")
    else:
        print("O número", numero, "é um numero Decimal")


def ex24():
    """Faça um Programa que leia 2 números e em seguida pergunte ao usuário qual
    operação ele deseja realizar. O resultado da operação deve ser acompanhado
    de uma frase que diga se o número é:par ou ímpar;positivo ou negativo;
    inteiro ou decimal."""

    numero1 = int(input("Insira o primeiro numero "))
    numero2 = int(input("Insira o segundoo numero "))

    print("Insira a operação")
    operação = int(input("1 - adição \n2 - Subtração \n3- Multiplicação \n4 - Divisão\n"))

    if operação == 1:
        result = numero1 + numero2
    elif operação == 2:
        result = numero1 - numero2
    elif operação == 3:
        result = numero1 * numero2
    elif operação == 4:
        result = numero1 / numero2
    else:
        print('Opção invalida')
        exit()

    print("_" * 30)
    print('Resultado = ', result)

    if result % 2 == 0:
        print("É um numero par")
    else:
        print("É um numero Impar")

    if result >= 0:
        print("É um número Positivo")
    else:
        print("É um número Negativo")

    if (result // 1 == result):
        print("O número", int(result), "é um numero inteiro")
    else:
        print("O número", result, "é um numero Decimal")


def ex25():
    """Faça um programa que faça 5 perguntas para uma pessoa sobre um crime. As perguntas são:
    "Telefonou para a vítima?"
    "Esteve no local do crime?"
    "Mora perto da vítima?"
    "Devia para a vítima?"
    "Já trabalhou com a vítima?" O programa deve no final emitir uma
    classificação sobre a participação da pessoa no crime. Se a pessoa
    responder positivamente a 2 questões ela deve ser classificada como
    "Suspeita", entre 3 e 4 como "Cúmplice" e 5 como "Assassino".
    Caso contrário, ele será classificado como "Inocente".
    """

    print("resposta com [s,n]")
    perguntas = [
        "Telefonou para a vítima?: ",
        "Esteve no local do crime?: ",
        "Mora perto da vítima?: ",
        "Devia para a vítima?: ",
        "Já trabalhou com a vítima?: "]
    lista = 0

    for status in perguntas:
        lista += (input(status) == "s")

    if lista == 5:
        print("Assassino")

    elif lista >= 3:
        print("Cúmplice")

    elif lista == 2:
        print("Suspeito")

    else:
        print("Inocente")


def ex26():
    """Um posto está vendendo combustíveis com a seguinte tabela de descontos:
    Álcool:
    até 20 litros, desconto de 3% por litro
    acima de 20 litros, desconto de 5% por litro
    Gasolina:
    até 20 litros, desconto de 4% por litro
    acima de 20 litros, desconto de 6% por litro Escreva um algoritmo que leia o
    número de litros vendidos, o tipo de combustível (codificado da seguinte forma:
    A-álcool, G-gasolina), calcule e imprima o valor a ser pago pelo cliente
    sabendo-se que o preço do litro da gasolina é R$ 2,50
    o preço do litro do álcool é R$ 1,90.
    """
    combustivel = input("Insira o tipo do combustivel [G, A]: ")
    litros = int(input("Insira a quantidade de litros "))
    alcool1 = (1.90 * 0.03)
    alcool2 = (1.90 * 0.05)
    gasolina1 = (2.50 * 0.04)
    gasolina2 = (2.50 * 0.06)

    print("Litros Vendidos: ", litros)

    if combustivel == 'a' or combustivel == 'A':
        print("Combustivel: alcool")
        if litros <= 20:
            resultado = (1.90 - alcool1) * litros
            print("Preço: ", resultado)
        else:
            resultado = (1.90 - alcool2) * litros
            print("Preço: ", resultado)
    elif combustivel == 'g' or combustivel == 'G':
        print("Combustivel: gasolina")
        if litros <= 20:
            resultado = (2.50 - gasolina1) * litros
            print("Preço: ", resultado)
        else:
            resultado = (2.50 - gasolina2) * litros
            print("Preço: ", resultado)
    else:
        print("Valor Inserido Invalido")


def ex27():
    """Uma fruteira está vendendo frutas com a seguinte tabela de preços:
    Até 5 Kg
    Acima de 5 Kg
    Morango
    R$ 2,50 por Kg
    R$ 2,20 por Kg
    Maçã
    R$ 1,80 por Kg
    R$ 1,50 por Kg   Se o cliente comprar mais de 8 Kg em frutas ou o valor total
    da compra ultrapassar R$ 25,00, receberá ainda um desconto de 10% sobre este
    total. Escreva um algoritmo para ler a quantidade (em Kg) de morangos e a
    quantidade (em Kg) de maças adquiridas e escreva o valor a ser pago pelo cliente.
    """
    morangos = int(input("Insira quantos kg de morango deseja "))
    macas = int(input("Insira quantos kg de maçã deseja  "))
    morango1 = morangos * 2.50
    morango2 = morangos * 2.20

    maca1 = macas * 1.80
    maca2 = macas * 1.50

    print("quantidade de maçãs: ", macas, "\nQuantidade de morangos: ", morangos)

    if morangos > 5:
        preco_morango = morango1
    else:
        preco_morango = morango2

    if macas > 5:
        preco_maca = maca1
    else:
        preco_maca = maca2

    total = preco_maca + preco_morango

    if total > 25 or (macas + morangos) > 8:
        print("Preço final: ", (total - (total * 0.1)))
    else:
        print("Preço final: ", total)


def ex28():
    """O Hipermercado Tabajara está com uma promoção de carnes que é imperdível.
    Confira:
    Até 5 Kg
    Acima de 5 Kg
    File Duplo
    R$ 4,90 por Kg
    R$ 5,80 por Kg
    Alcatra
    R$ 5,90 por Kg
    R$ 6,80 por Kg
    Picanha
    R$ 6,90 por Kg
    R$ 7,80 por Kg Para atender a todos os clientes, cada cliente poderá levar
    apenas um dos tipos de carne da promoção, porém não há limites para a
    quantidade de carne por cliente. Se compra for feita no cartão Tabajara o
    cliente receberá ainda um desconto de 5% sobre o total da compra. Escreva um
    programa que peça o tipo e a quantidade de carne comprada pelo usuário e gere um
    cupom fiscal, contendo as informações da compra: tipo e quantidade de carne,
    preço total, tipo de pagamento, valor do desconto e valor a pagar."""

    print("Carnes: File Duplo, Alcatra , Picanha")
    carne = input("Insira a carne que deseja comprar: ")
    quantidade_carne = int(input("Insira a quantidade de carne que irá comprar: "))
    pagamento = input("Qual tipo de pagamento: ")

    print("Tipo de carne:", carne, "\nQuantidade de carne:", quantidade_carne)

    preco_file_duplo1 = quantidade_carne * 4.90
    preco_file_duplo2 = quantidade_carne * 5.80

    alcatra1 = quantidade_carne * 5.90
    alcatra2 = quantidade_carne * 6.80

    picanha1 = quantidade_carne * 6.90
    picanha2 = quantidade_carne * 7.80

    if carne == "filé duplo":
        if quantidade_carne > 5:
            preco_total = preco_file_duplo1
        else:
            preco_total = preco_file_duplo2
    elif carne == "alcatra":
        if quantidade_carne > 5:
            preco_total = alcatra1
        else:
            preco_total = alcatra2
    elif carne == "picanha":
        if quantidade_carne > 5:
            preco_total = picanha1
        else:
            preco_total = picanha2
    else:
        print("Carne Invalida")

    if pagamento == "Cartão Tabajara":
        desconto = preco_total * 0.05
        print("Tipo de pagamento: ", pagamento)
        print("Valor do desconto: ", desconto)
        print("Valor Final: ", (preco_total - desconto))
    else:
        print("Tipo de pagamento: normal\nValor do desocnto: 0")
        print("Valor Final: ", preco_total)





























