# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@pedro1hen1'


def ex01(n):
    """Faça um programa para imprimir:
    1
    2   2
    3   3   3
    .....
    n   n   n   n   n   n  ... n
    para um n informado pelo usuário. Use uma função que receba um valor n
    inteiro e imprima até a n-ésima linha."""
    for i in range(n + 1):
        lista = ''
        for c in range(i):
            lista += str(i) + "\t"
        print(lista)
        print("_" * 30)


numero = int(input("Insira o numero "))
ex01(numero)


def ex02(numero, lista):
    """Faça um programa para imprimir:
    1
    1   2
    1   2   3
    .....
    1   2   3   ...  n
    para um n informado pelo usuário. Use uma função que receba um valor n
    inteiro imprima até a n-ésima linha"""
    for i in range(numero):
        lista += str(i + 1) + "\t"
        print(lista)
        print("_" * 30)


lista = ''
numero = int(input("Insira o numero "))
ex02(numero, lista)


def ex03(arg1, arg2, arg3):
    """Faça um programa, com uma função que necessite de três argumentos
    e que forneça a soma desses três argumentos."""
    print("\nSoma =", arg1 + arg2 + arg3)
    print("_" * 30)


vetor = []
for c in range(3):
    print("\nArgumento ", c + 1)
    arg = vetor.append(float(input("Insira o argumento: ")))
ex03(vetor[0], vetor[1], vetor[2])


def ex04(n):
    """Faça um programa, com uma função que necessite de um argumento.
    A função retorna o valor de caractere ‘P’, se seu argumento for positivo,
    e ‘N’, se seu argumento for zero ou negativo."""
    if n > 0:
        print("P")
        print("_" * 30)
    else:
        print("N")
        print("_" * 30)


numero = float(input("Insira um numero "))
ex04(numero)


def ex05(taxa_imposto, custo):
    """Faça um programa com uma função chamada soma_imposto.
    A função possui dois parâmetros formais: taxa_imposto, que é a quantia
    de imposto sobre vendas expressa em porcentagem e custo, que é o custo
    de um item antes do imposto. A função “altera” o valor de custo para
    incluir o imposto sobre vendas."""
    return custo + custo * (float(taxa_imposto) / 100)


custo = float(input("Insira o custo do produto "))
taxa_imposto = int(input("Insira a taxa de imposto: "))

print("Valor do custo R$: ", ex05(taxa_imposto, custo))
print("_" * 30)

def ex06(hora):
    """Faça um programa que converta da notação de 24 horas para a notação
    de 12 horas. Por exemplo, o programa deve converter 14:25 em 2:25 P.M. A
    entrada é dada em dois inteiros. Deve haver pelo menos duas funções: uma
    para fazer a conversão e uma para a saída. Registre a informação A.M./P.M.
    como um valor ‘A’ para A.M. e ‘P’ para P.M. Assim, a função para efetuar as
    conversões terá um parâmetro formal para registrar se é A.M. ou P.M. Inclua um
    loop que permita que o usuário repita esse cálculo para novos valores de
    entrada todas as vezes que desejar.
    """
    hora_12 = hora - 12
    return hora_12


def ex06_1(hora_12, hora, minutos):
    if hora >= 12 and hora <= 24:
        print(hora_12, ":", minutos, "PM")
        print("_" * 30)
    else:
        print(hora, ":", minutos, "AM")
        print("_" * 30)



hora = int(input("\nInsira a hora "))
minutos = int(input("Insira os minutos "))
hora_12 = ex06(hora)
ex06_1(hora_12, hora, minutos)



def ex07(prestacao, atraso):
    juros = prestacao
    multa = prestacao * 0.03
    if atraso > 0:
        i = 0
        while i < atraso:
            juros = juros + juros * 0.001
            i += 1
        juros = juros - prestacao
        return float(multa + prestacao + juros)
    else:
        return float(prestacao)


prestacao = int(input("Insira o valor da prestação "))
atraso = int(input("Quantos dias de atraso "))
print("Total: R$", round(ex07(prestacao, atraso)))
print("_" * 30)


def ex08(numero):
    """Faça uma função que informe a quantidade de dígitos de
    um determinado número inteiro informado"""
    caracteres = str(numero)
    print("O numero tem ", len(caracteres), "digitos")


numero = int(input("Insira um numero inteiro "))
ex08(numero)


def ex09(numero):
    """Reverso do número. Faça uma função que retorne o reverso
    de um número inteiro informado. Por exemplo: 127 -> 721."""
    numeroInvertido = int(str(numero)[::-1])
    print(numeroInvertido)
    print("_" * 30)


numero = int(input("Insira um numero "))
ex09(numero)


def ex11(data):
    mesesStr = ["Janeiro", "Fevereiro", "Marco", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro",
                "Novembro", "Dezembro"]
    dia = int(data[0:2])
    mes = int(data[3:5])
    ano = int(data[6:10])
    print(dia, "de", mesesStr[mes - 1], "de", ano)


entrada = input("Insira uma data (DD/MM/AAAA): ")
ex11(entrada)
print("_" * 30)

import random


def ex12(entrada):
    """Construa uma função que receba uma string como parâmetro e devolva outra
    string com os carateres embaralhados. Por exemplo: se função receber a
    palavra python, pode retornar npthyo, ophtyn ou qualquer outra combinação
    possível, de forma aleatória. Padronize em sua função que todos os
    caracteres serão devolvidos em caixa alta ou caixa baixa,
    independentemente de como foram digitados."""

    palavra_embaralhada = ''.join(random.sample(entrada, len(entrada)))
    print(palavra_embaralhada)
    print("_" * 30)


entrada = str(input("Insira uma palavra "))
ex12(entrada)

altura = int(input("Insira a altura da moldura "))
largura = int(input("Insira a largura da moldura "))
while altura < 1 or altura > 20 or largura < 1 or largura > 20:
    print("\n" * 3, "erro[Somente numeros entre 1 e 20")
    altura = int(input("Insira a altura dda moldura  "))
    largura = int(input("Insira a largura da moldura  "))


def ex13(largura):
    """Construa uma função que desenhe um retângulo usando os caracteres
    ‘+’ , ‘−’ e ‘| ‘. Esta função deve receber dois parâmetros, linhas e
    colunas, sendo que o valor por omissão é o valor mínimo igual a 1 e o
    valor máximo é 20. Se valores fora da faixa forem informados,
    eles devem ser modificados para valores dentro da faixa de forma elegante."""
    print("+", "  -  " * (largura - 2), "+")


def ex13_1(altura, largura):
    for i in range(altura - 2):
        print("|", "     " * (largura - 2), "|")


def ex13_2(altura, largura):
    ex13(largura)
    ex13_1(altura, largura)
    ex13(largura)


ex13_2(altura, largura)
