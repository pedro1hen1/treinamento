# /bin/env python
# -*- encode: utf-7 -*-
__author__ = '@pedro2hen1'

def ex01():
    """Faça um Programa que leia um vetor de 5 números inteiros e mostre-os.
    """
    lista = []
    print ('Insira 5 numeros')
    for i in range(5):
        lista.append(input(" Numero "))
    print ("->",lista)

def ex02():
    """Faça um Programa que leia um vetor de 10 números reais e mostre-os 
    na ordem inversa."""
    lista = []
    print ('Insira os 10 numeros reais')
    for i in range(10):
     	lista.append(float(input('Numero ')))
    lista=lista[::-1]
    print("_"*80)
    print ("->",lista) 
    
def ex03():
    """Faça um Programa que leia 4 notas, mostre as notas e a média na tela."""
    lista = []
    media = 0
    print ('Insira as 4 notas')
    for c in range(4):
    	lista.append(float(input('Nota '+ str(c+1) +':')))
    	media += lista[c]
    media = media/4
    print (lista) 
    print (media)
    
def ex04():
    """Faça um Programa que leia um vetor de 10 caracteres, e diga quantas 
    consoantes foram lidas. Imprima as consoantes."""
    vogais = ['a', 'e', 'i', 'o', 'u']
    lista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
    c = 0
    while c < 5:
        if vogais[c] in lista:
            #remove as vogais 
            lista.remove(vogais[c])
        c += 1
    print("\nNo vetor tem", len(lista), "consoantes")
    print("-> ", lista)
    
def ex05():
    """Faça um Programa que leia 20 números inteiros e armazene-os num vetor. 
    Armazene os números pares no vetor PAR e os números IMPARES no vetor impar.
    Imprima os três vetores."""
    lNumeros = []
    lPar = []
    lmpar = []
    
    numero = 0
    print('Insira os numeros:')
    for i in range(20):
     lNumeros.append((int(input('Numero' + str(i+1) +':'))))
     numero = lNumeros[i]
     print (numero)
     if(numero%2 == 0):
     	lPar.append(numero)
     else:
     	lmpar.append(numero)
    
    print("numero inseridos ->",lNumeros)
    print("Lista Numeros par ->",lPar)
    print("Lista Numeros impares ->",lmpar)
    
def ex06():
    """Faça um Programa que peça as quatro notas de 10 alunos, calcule e 
    armazene num vetor a média de cada aluno, imprima o número de alunos com 
    média maior ou igual a 7.0."""
    notasAluno = []
    notas = []
    media2 = []
    print("Programa se encerra depois que inserir 4 notas para cada aluno!")
    for i in range(10):
        media = 0
        notasAluno = []
        print('Aluno: ' + str(i + 1))
        for c in range(4):
            notasAluno.append(float(input('Nota: ' + str(c + 1) + '\n')))
            media += notasAluno[c]
        media = media / 4
        notas.append(media)
        if media >= 7:
            media2.append(media)
     
    print("media dos alunos ->", notas)
    print("_"*80)
    print("Quantidade de alunos com media maior que 7 -> ", len(media2))
    
def ex07():
    """Faça um Programa que leia um vetor de 5 números inteiros,
    mostre a soma, a multiplicação e os números."""
    lista = []
    soma = 0
    print ('Insira as 4 notas')
    for c in range(5):
    	lista.append(float(input('numero '+ str(c+1) +':')))
    	soma += lista[c]
    mult = soma * 5	
    
    print ("Numeros inseridos ->",lista) 
    print("Soma dos numeros inseridos ->",soma)
    print("Multiplicação dos Numeros inseridos ->",mult)

    
def ex08():
    """Faça um Programa que peça a idade e a altura de 5 pessoas, 
    armazene cada informação no seu respectivo vetor. Imprima a idade e a 
    altura na ordem inversa a ordem lida.
    """
    idade = []
    altura = []
    n_pessoa = 1
    for i in range(5):
        print("\nPessoa ", n_pessoa)
        c_idade = idade.append(int(input("Insira sua idade ")))
        c_altura = altura.append(float(input("Insira sua altura: ")))
        n_pessoa += 1
        
    idade=idade[::-1]
    altura=altura[::-1]
    print("Idades-> ", idade)
    print("Altura-> ", altura)
    
def ex09():
    """Faça um Programa que leia um vetor A com 10 números inteiros, calcule 
    e mostre a soma dos quadrados dos elementos do vetor."""
    lista = []
    print("_"*30)
    print("Para inicar insira um numero")
    for c in range(0, 10):
        lista.append(int(input("Numero " + str(c+1) +":" )))
        quadrado = 0
        for numero in lista:
            quadrado += (numero * numero)
    print ("O resultado dos numeros ao quadrado -> ", quadrado)

def ex10():
    """Faça um Programa que leia dois vetores com 10 elementos cada. Gere um 
    terceiro vetor de 20 elementos, cujos valores deverão ser compostos pelos 
    elementos intercalados dos dois outros vetores."""
    vetor1 = []
    vetor2 = []
    vetor3 = []
    print("_"*40)
    print("Para iniciar insira os numeros no primeiro vetor")
    
    for i in range(0, 10):
        vetor1.append(int(input("Numero " + str(i+1) +":" )))
    print('Insira os numeros do segundo vetor')
    
    for i in range(0, 10):
        vetor2.append(int(input("Numero " + str(i+1) +":" )))
    
    for i in range(0, 10):
        vetor3.append(vetor1[i])
        vetor3.append(vetor2[i])
    
    print("Numeros intercalados ->",vetor3)

def ex11():
    """Altere o programa anterior, intercalando 3 vetores de 10 elementos cada."""
    vetor1 = []
    vetor2 = []
    vetor3 = []
    vetor4 = []
    print("_"*40)
    print("Para iniciar insira os numeros no primeiro vetor")
    
    for i in range(0, 10):
            vetor1.append(int(input("Numero " + str(i+1) +":" )))
    print("Insira os numeros do segundo vetor")
    for i in range(0, 10):
            vetor2.append(int(input("Numero " + str(i+1) +":" )))
    print ("Insira os numeros do segundo vetor")
    for i in range(0, 10):
        vetor3.append(int(input("Numero " + str(i+1) +":")))
    for i in range(0, 10):
         vetor4.append(vetor1[i])
         vetor4.append(vetor2[i])
         vetor4.append(vetor3[i])
    
     print("Numeros intercalados ->",vetor4)

def ex12():
    """Foram anotadas as idades e alturas de 30 alunos. Faça um Programa que 
    determine quantos alunos com mais de 13 anos possuem altura inferior à 
    média de altura desses alunos."
    
    
    
    
    
    
    
    
    
    
    
    
    
    